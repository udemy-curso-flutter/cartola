import 'dart:convert';

import 'package:http/http.dart' as http;

class CartolaService {

  Map<String, String> _apiUrls = {
    "statusMercado": "https://api.cartolafc.globo.com/mercado/status",
    "destaquesMercado": "https://api.cartolafc.globo.com/mercado/destaques",
    "rodadas": "https://api.cartolafc.globo.com/rodadas",
    "partidas": "https://api.cartolafc.globo.com/partidas/?",
    "clubes": "https://api.cartolafc.globo.com/clubes",
    "atletasMercado": "https://api.cartolafc.globo.com/atletas/mercado",
    "timesCartola": "https://api.cartolafc.globo.com/times?q=text",
    "ligasCartola": "https://api.cartolafc.globo.com/ligas?q=text",
  };

  http.Response response;

  static final CartolaService _instance = CartolaService.internal();

  factory CartolaService() => _instance;

  CartolaService.internal();

  Future<Map> getStatusMercado() async {
    response = await http.get(_apiUrls["statusMercado"]);
    return json.decode(response.body);
  }

  Future<List<dynamic>> getDestaquesMercado() async {
    response = await http.get(_apiUrls["destaquesMercado"]);
    return json.decode(response.body);
  }

  Future<Map> getRodadas() async {
    response = await http.get(_apiUrls["rodadas"]);
    return json.decode(response.body);
  }

  Future<Map> getPartidasPorRodada(int rodada) async {
    response = await http.get(_apiUrls["partidas"].replaceAll('text', rodada.toString()));
    return json.decode(response.body);
  }

  Future<Map> getClubes() async {
    response = await http.get(_apiUrls["clubes"]);
    return json.decode(response.body);
  }

  Future<Map> getAtletasMercado() async {
    response = await http.get(_apiUrls["atletasMercado"]);
    return json.decode(response.body);
  }

  Future<List<dynamic>> getTimePorNome(String time) async {
    response = await http.get(_apiUrls["timesCartola"].replaceAll('text', time.toString()));
    return json.decode(response.body);
  }

  Future<Map> getLigaPorNome(String liga) async {
    response = await http.get(_apiUrls["ligasCartola"].replaceAll('?', liga.toString()));
    return json.decode(response.body);
  }

}
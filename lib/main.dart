import 'package:cartola/pages/home.page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart'
  show debugDefaultTargetPlatformOverride;

void main() {

  debugDefaultTargetPlatformOverride = TargetPlatform.fuchsia;

  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    title: "Cartola",
    home: Home(),
    theme: ThemeData(
      primarySwatch: Colors.green,
      primaryColor: Colors.green[800],
      accentColor: Colors.greenAccent,
    ),
  ));
}
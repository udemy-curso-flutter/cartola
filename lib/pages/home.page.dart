import 'package:cartola/drawer/custom.drawer.dart';
import 'package:cartola/pages/dashboard.page.dart';
import 'package:cartola/pages/mercado.page.dart';
import 'package:cartola/pages/times.page.dart';
import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<Home>  {

  List title = ["Dashboard", "Mercado"];

  PageController _pageController = PageController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Cartola"),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.refresh,
              color: Colors.white,
            ),
            onPressed: () {},
          ),
        ],
      ),
      backgroundColor: Colors.grey[100],
      body: PageView(
        controller: _pageController,
        physics: NeverScrollableScrollPhysics(),
        children: <Widget>[
          DashboardPage(),
          MercadoPage(),
          TimesPage(),
        ],
      ),
      drawer: CustomDrawer(_pageController),
    );
  }

}
import 'package:cartola/service/cartola.service.dart';
import 'package:flutter/material.dart';

class MercadoPage extends StatefulWidget {
  @override
  _MercadoPageState createState() => _MercadoPageState();
}

class _MercadoPageState extends State<MercadoPage> {

  CartolaService _cartolaService = CartolaService();

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Expanded(
          child: FutureBuilder(
            future: _cartolaService.getAtletasMercado(),
            builder: (context, snapshot) {
              switch (snapshot.connectionState) {
                case ConnectionState.none: 
                case ConnectionState.waiting:
                  return Container(
                    width: 200.0,
                    height: 200.0,
                    alignment: Alignment.center,
                    child: CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation<Color>(Theme.of(context).primaryColor),
                      strokeWidth: 5.0,
                    ),
                  );
                  break;
                default:
                  if (snapshot.hasError) {
                    return Container();
                  } else {
                    return _builderMercado(context, snapshot.data["atletas"]);
                  }
              }
            },  
          ),
        ),
      ],
    );
  }

  Widget _builderMercado(BuildContext context, List<dynamic> data) {
    return ListView.builder(
      itemCount: data.length,
      itemBuilder: (context, index) {
        return _cardMercado(data, index);
      },
    );
  }

  String _printScout(Map<String, dynamic> scoutMap) {
    return (scoutMap.isNotEmpty) ? scoutMap.toString() : "";
  }

  Widget _cardMercado(dynamic data, int index) {
    return Card(
      child: Padding(
        padding: EdgeInsets.all(10.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              height: 40.0,
              width: 40.0,
              decoration: BoxDecoration(
                shape: BoxShape.rectangle,
                image: DecorationImage(
                  fit: BoxFit.cover,
                  image: AssetImage("images/person.png"),
                ),
              ),
            ),
            Expanded(
              child: Padding(
                padding: EdgeInsets.only(left: 10.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      data[index]["apelido"],
                      style: TextStyle(
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold,
                        color: Colors.grey[400],
                      ),
                    ),
                    Text(
                      _printScout(data[index]["scout"]),
                      style: TextStyle(
                        fontSize: 15.0,
                        color: Colors.grey[400],
                      ),
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                    ),
                  ],
                ),
              ),
            ),            
          ],
        ),
      ),
    );
  }
}
import 'package:cartola/service/cartola.service.dart';
import 'package:flutter/material.dart';

class TimesPage extends StatefulWidget {
  @override
  _TimesPageState createState() => _TimesPageState();
}

class _TimesPageState extends State<TimesPage> {

  TextEditingController _editingController = TextEditingController();
  String _textSearch;

  CartolaService _cartolaService = CartolaService();

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10.0),
      child: Column(
        children: <Widget>[
          TextField(
            controller: _editingController,
            decoration: InputDecoration(
              labelText: "Pesquisar por Nome",
              hintText: "Pesquisar por Nome",
              prefixIcon: Icon(Icons.search),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(5.0)),
              ),
            ),
            onChanged: (value) {
              setState(() {
                _textSearch = value;                
              });
            },
          ),
          Expanded(
            child: FutureBuilder(
              future: _cartolaService.getTimePorNome(_textSearch),
              builder: (context, snapshot) {
                switch (snapshot.connectionState) {
                  case ConnectionState.none: 
                  case ConnectionState.waiting:
                    return Container(
                      width: 200.0,
                      height: 200.0,
                      alignment: Alignment.center,
                      child: CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation<Color>(Theme.of(context).primaryColor),
                        strokeWidth: 5.0,
                      ),
                    );
                    break;
                  default:
                    print(_textSearch);
                    if (snapshot.hasError || _textSearch == null || _textSearch == "" || _textSearch.length < 3) {
                      return Container();
                    } else {
                      return _builderTimes(context, snapshot.data);
                    }
                }
              },  
            ),
          ),
        ],
      ),
    );
  }

  Widget _builderTimes(BuildContext context, List<dynamic> data) {
    return ListView.builder(
      itemCount: data.length,
      itemBuilder: (context, index) {
        return _cardTimes(data, index);
      },
    );
  }

  dynamic _loadImage(String image) {
    try {
      return NetworkImage(image);
    } catch(e) {
      return AssetImage("images/person.png");
    }
  }

  Widget _cardTimes(dynamic data, int index) {
    return Card(
      child: Padding(
        padding: EdgeInsets.all(10.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              height: 40.0,
              width: 40.0,
              decoration: BoxDecoration(
                shape: BoxShape.rectangle,
                image: DecorationImage(
                  fit: BoxFit.cover,
                  image: _loadImage(data[index]["url_escudo_png"]),
                ),
              ),
            ),
            Expanded(
              child: Padding(
                padding: EdgeInsets.only(left: 10.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      data[index]["nome"],
                      style: TextStyle(
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold,
                        color: Colors.grey[400],
                      ),
                    ),
                    Text(
                      data[index]["nome_cartola"],
                      style: TextStyle(
                        fontSize: 15.0,
                        color: Colors.grey[400],
                      ),
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                    ),
                  ],
                ),
              ),
            ),            
          ],
        ),
      ),
    );
  }

}
import 'package:cartola/drawer/drawer.tile.dart';
import 'package:flutter/material.dart';

class CustomDrawer extends StatelessWidget {

  PageController _pageController = PageController();

  CustomDrawer(this._pageController);

  Widget _buildBodyBack(BuildContext context) => Container(
    decoration: BoxDecoration(
      gradient: LinearGradient(
        colors: [
          Theme.of(context).primaryColor,
          Theme.of(context).accentColor,
        ],
        begin: Alignment.topCenter,
        end: Alignment.bottomCenter
      ),
    ),
  );

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Stack(
        children: <Widget>[
          _buildBodyBack(context),
          ListView(
            padding: EdgeInsets.only(top: 80.0, left: 40.0),
            children: <Widget>[
              Text(
                "Cartola",
                style: TextStyle(
                  fontSize: 30.0,
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                ),
              ),
              Text(
                "Consumindo Api do CartolaFC",
                style: TextStyle(
                  fontSize: 15.0,
                  color: Colors.white,
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 20.0, bottom: 20.0),
                child: Divider(color: Colors.white,),
              ),
              DrawerTile(Icons.home, "Início", _pageController, 0),
              DrawerTile(Icons.business_center, "Mercado", _pageController, 1),
              //DrawerTile(Icons.date_range, _pageController, 2),
              DrawerTile(Icons.group, "Times no Cartola", _pageController, 3),
              //DrawerTile(Icons.insert_chart, _pageController, 4),
            ],
          ),
        ],
      ),
    );
  }
}